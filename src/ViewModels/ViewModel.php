<?php

namespace FDP\Common\ViewModels;

use SilverStripe\ORM\DataObjectInterface;
use SilverStripe\View\ViewableData;

class ViewModel extends ViewableData implements DataObjectInterface
{
    public function __construct($values = array())
    {
        parent::__construct();
        foreach ($values as $k => $v) {
            $this->$k = $v;
        }
    }

    public function __get($key)
    {
        $method = sprintf('get%s', $key);
        if ($this->hasMethod($method)) {
            return $this->$method();
        } else {
            return parent::__get($key);
        }
    }

    public function __set($key, $value)
    {
        $method = sprintf('set%s', $key);
        if ($this->hasMethod($method)) {
            return $this->$method($value);
        } else {
            return parent::__set($key, $value);
        }
    }

    public function write()
    {
    }

    public function delete()
    {
    }

    public function setCastedField($field, $value)
    {
        $this->$field = $value;
    }
}
