<?php

namespace FDP\Common\Emails;

use Pelago\Emogrifier;

use SilverStripe\Control\Email\Email;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\View\Requirements;

class ThemedEmail extends Email
{
    private static $template = 'ThemedEmail';

    public function renderWith($template, $data = null)
    {
        if (is_null($data)) {
            $data = [];
        }
        $data = array_merge($data, $this->getData());
        Requirements::clear();
        $data['Body'] = parent::renderWith($template, $data);
        $body = parent::renderWith($this->stat('template'), $data);
        Requirements::restore();
        if (preg_match('/<style[^>]*>(?:<\!--)?(.*)(?:-->)?<\/style>/ims', $body, $match)) {
            $css = $match[1];
            $html = str_replace(
                array(
                    "<p>\n<table>",
                    "</table>\n</p>",
                    '&copy ',
                    $match[0],
                ),
                array(
                    "<table>",
                    "</table>",
                    '',
                    '',
                ),
                $body
            );
            $emog = new Emogrifier($html, $css);
            $body = $emog->emogrify();
        }
        return $body;
    }

    public function output()
    {
        return new HTTPResponse($this->renderWith($this->getHTMLTemplate()));
    }
}
