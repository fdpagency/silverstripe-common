<?php

namespace FDP\Common\Extensions;

use FDP\Common\Utilities\StringUtilities;

use SilverStripe\Core\Convert;
use SilverStripe\Core\Extension;
use SilverStripe\ORM\ArrayList;

class DBString extends Extension
{
    public function Slugify($separator = '-')
    {
        return StringUtilities::form_slug($this->owner->value, $separator);
    }
}
