<?php

namespace FDP\Common\Utilities;

use InvalidArgumentException;
use SilverStripe\Control\Director;
use SilverStripe\Core\Path;
use SilverStripe\View\SSViewer;

class Requirements_Backend extends \SilverStripe\View\Requirements_Backend
{
    public function themedJavascript($name, $type = null)
    {
        $opts = [];
        if ($type) {
            $opts['type'] = $type;
        }
        $path = '';
        try {
            return parent::themedJavascript($name, $type);
        } catch (InvalidArgumentException $ex) {
            if (Director::isDev()) {
                $path = Path::join(
                    ['/resources', 'themes', SSViewer::get_themes()[0], 'javascript', $name]
                );
            } else {
                throw $ex;
            }
        }
        return $this->javascript($path, $opts);
    }
}
