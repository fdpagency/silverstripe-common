<?php

namespace FDP\Common\Tests\Utilities;

use FDP\Common\Traits\TestUtilities;
use FDP\Common\Utilities\StringUtilities;

use SilverStripe\Dev\SapphireTest;

class StringUtilitiesTest extends SapphireTest
{
    use TestUtilities;

    public function testStringUtilitiesSlugify()
    {
        $this->assertEquals(
            '123-string-and-has-characters',
            StringUtilities::form_slug('123 string & has £@($%(@(@ characters')
        );
    }
}
