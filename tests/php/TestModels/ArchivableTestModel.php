<?php

namespace FDP\Common\Tests\TestModels;

use SilverStripe\Dev\TestOnly;
use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;

class ArchivableTestModel extends DataObject implements TestOnly
{
    private static $table_name = 'ArchivableTestModel';
    private static $extensions = [
        Versioned::class
    ];

    private static $db = [
        'Archivable' => 'Boolean'
    ];

    public function canArchive($member = null)
    {
        return $this->Archivable;
    }
}
